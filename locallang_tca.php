<?php

$LOCAL_LANG = Array (
    'default' => Array (
          	'filepath'	=> 'Filepath',
			'view'		=> 'Destination',
			'gallery'	=> 'Gallery',
			'random'	=> 'Random',
			'galleryid'	=> 'Gallery ID'
    ),

    'de' => Array (
          	'filepath'	=> 'Dateipfad (wenn nicht im typoscript angegeben)',
			'view'		=> 'Ansicht',
			'gallery'	=> 'Galerie',
			'random'	=> 'Random',
			'galleryid'	=> 'Galerie ID'
    )

);

?> 
$(document).ready(function() {
	//$('.fancybox').fancybox();

	$('.fancybox').fancybox({
		prevEffect : 'none',
		nextEffect : 'none',

		closeBtn  : true,
		arrows    : true,
		nextClick : true,

		helpers : {
			thumbs : {
				width  : 50,
				height : 50
			}
		}
	});
});
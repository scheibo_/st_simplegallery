<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2012 Thomas Scheibitz <mail@kreativschmiede-eichsfeld.de>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 * Hint: use extdeveval to insert/update function index above.
 */

require_once(PATH_tslib.'class.tslib_pibase.php');


/**
 * Plugin 'Simple Gallery' for the 'st_simplegallery' extension.
 *
 * @author	Thomas Scheibitz <mail@kreativschmiede-eichsfeld.de>
 * @package	TYPO3
 * @subpackage	tx_stsimplegallery
 */
class tx_stsimplegallery_pi1 extends tslib_pibase {
	var $prefixId      = 'tx_stsimplegallery_pi1'; // Same as class name
	var $scriptRelPath = 'pi1/class.tx_stsimplegallery_pi1.php'; // Path to this script relative to the extension dir.
	var $extKey        = 'st_simplegallery'; // The extension key.
	var $pi_checkCHash = false;
	
	/**
	 * The main method of the PlugIn
	 *
	 * @param	string		$content: The PlugIn content
	 * @param	array		$conf: The PlugIn configuration
	 * @return	The content that is displayed on the website
	 */
	function main($content, $conf) {
		$this->conf = $conf;
		$this->pi_setPiVarDefaults();
		$this->pi_loadLL();
		$this->pi_initPIflexForm();

		$view = $this->cObj->data['pi_flexform']['data']['sDEF']['lDEF']['view']['vDEF'];
		$filepath = $this->cObj->data['pi_flexform']['data']['sDEF']['lDEF']['filepath']['vDEF'];

		if($view == 0) {
			if(t3lib_div::_GET('tx_stsimplegallery_pi1')) {
				$content = $this->listview($conf['listview.'], $filepath);
			} else {
				$content = $this->overview($conf['overview.'], $filepath);
			}
		} else {
			$content = $this->random($conf['randomview.'], $filepath);
		}
		return $this->pi_wrapInBaseClass($content);
	}

	/**
	 * Generate the overview
	 * @param $conf array
	 * @param $filepath string
	 * @return string
	 */
	protected function overview($conf, $filepath) {
		// setup the pagebrowser
		$ov_max = $conf['maxitems'];
		$galleryfolders = ($conf['listreverse'] == 1) ? array_reverse(t3lib_div::get_dirs($filepath)) : t3lib_div::get_dirs($filepath);
		$pageid = $this->cObj->data['pid'];
		$gallerypage = t3lib_div::_GET('page');
		$backpage = ($gallerypage) ? '&tx_stsimplegallery_pi1[backpage]=' . $gallerypage : '';
		$seeitem_start = '';
		$seeitem_end = '';

		if(count($galleryfolders) > $ov_max) {
			$seeitem_start = $ov_max*t3lib_div::_GET('page');
			$seeitem_end = $seeitem_start+$ov_max-1;
		}

		// maintitle
		$content = $this->cObj->cObjGetSingle($conf['maintitle'], $conf['maintitle.']);
		$i = 1;
		foreach ($galleryfolders as $key => $value) {
			$galleryname = $this->getGalleryTitle('overview.', $filepath, $value);

			// Previewimage
			$allimages = t3lib_div::getFilesInDir($filepath . $value, 'jpg', true);
			$previewimagepath = $allimages[array_rand($allimages)];
			$fileArray = array(
				'imageTitle' => $galleryname,
				'altTitle' => $galleryname,
				'imageFile' => $previewimagepath,
				'gallerytitle' => $galleryname,
				'parameter' => $pageid,
				'additionalParams' => '&tx_stsimplegallery_pi1[gallery]=' . $value . $backpage
			);
			$this->cObj->start($fileArray);
			$previewimage = $this->cObj->cObjGetSingle($conf['image'], $conf['image.']);

			// Pfüfen  der Anzahl der anzuzeigenen Ordner
			if($ov_max > 0  && count($galleryfolders) > $ov_max) {
				if($key >= $seeitem_start && $key <= $seeitem_end) {
					$content .= $previewimage;
				}
			} else {
				$content .= $previewimage;
			}
			$i++;
		}
		$numberofpages = ceil(count($galleryfolders)/$ov_max);
		$content .= $this->getListGetPageBrowser($numberofpages, 'page');
		return $content;
	}

	/**
	 * Generate the listview
	 * @param $conf array
	 * @param $filepath string
	 * @return string
	 */
	protected function listview($conf, $filepath) {
		$pageid = $this->cObj->data['pid'];
		$thegallery = t3lib_div::_GET('tx_stsimplegallery_pi1');
		$gallery = $thegallery['gallery'];
		$thebackpage = t3lib_div::_GET('tx_stsimplegallery_pi1');
		$backpage = $thebackpage['backpage'];
		$seeitem_start = '';
		$seeitem_end = '';

		$backlinkconf = array(
			'banklinktext' => $this->pi_getLL('back'),
			'parameter' => $pageid,
			'additionalParams' => '&page=' . $backpage
		);
		$this->cObj->start($backlinkconf);
		$backToOverview = $this->cObj->cObjGetSingle($conf['backlink'], $conf['backlink.']);

		$images = array_values(t3lib_div::getFilesInDir($filepath . $gallery, 'jpg', true));
		$lv_max = $conf['maxitems'];
		if(count($images) > $lv_max) {
			$seeitem_start 	= $lv_max*t3lib_div::_GET('lv');
			$seeitem_end	= $seeitem_start+$lv_max-1;
		}

		$galleryname = $this->getGalleryTitle('listview.', $filepath, $gallery, 1);

		// Read TXT file and cet line in array
		$txtfile = reset(t3lib_div::getFilesInDir($filepath . $gallery, 'txt', true));
		if($txtfile) {
			$txtcontent = $this->cObj->fileResource($txtfile);
			$txtarray = preg_split("/\r\n|\r|\n/", $txtcontent);
		}

		$content = $galleryname;
		$content .= $backToOverview;
		$i = 1;
		$imagecontent = '';
		foreach ($images as $key => $value) {

			// Bildunterschrift
			if(isset($txtarray[$i-1])) {
				$alttag = $txtarray[$i-1];
			} else {
				$alttag = '';
			}

			$fileArray = array(
				'imageTitle' => $alttag,
				'altTitle' => $alttag,
				'imageFile' => $value,
				'gallerytitle' => $alttag,
				'imagedescription' => $alttag
			);
			$this->cObj->start($fileArray);
			$image = $this->cObj->cObjGetSingle($conf['image'], $conf['image.']);
			if($lv_max > 0 && count($images) > $lv_max) {
				if($key >= $seeitem_start && $key <= $seeitem_end) {
					$imagecontent .= $image;
				}
			} else {
				$imagecontent .= $image;
			}
			$i++;
		}

		$content .= $imagecontent;
		$content .= $backToOverview;
		$content .= $this->getListGetPageBrowser(ceil(count($images)/$lv_max),'lv');
		return $content;
	}

	/**
	 * Generate the Randomview
	 * @param $conf array
	 * @param $filepath string
	 * @return string
	 */
	protected function random($conf, $filepath) {

		$galleryPageID = $this->cObj->data['pi_flexform']['data']['sDEF']['lDEF']['galleryid']['vDEF'];
		$folders = ($conf['listreverse'] == 1) ? array_reverse(t3lib_div::get_dirs($filepath)) : t3lib_div::get_dirs($filepath);
		$randomfolder = $folders[array_rand($folders)];
		$images = array_values(t3lib_div::getFilesInDir($filepath . $randomfolder, 'jpg', true));
		$random_items = ($conf['maxitems'] < count($images) || $conf['maxitems'] <= 1) ? $conf['maxitems'] : count($images);
		$random_var = array_rand($images, $random_items);
		$i = 1;
		$content = $this->cObj->cObjGetSingle($conf['maintitle'], $conf['maintitle.']);
		for($x = 0; $x < count($random_var); $x++) {
			$fileArray = array(
				'imageFile' => $images[$random_var[$x]],
				'parameter' => $galleryPageID,
				'additionalParams' => '&tx_stsimplegallery_pi1[gallery]=' . $randomfolder,
				'toAllGalleryText' => $this->pi_getLL('togallery')
			);
			$this->cObj->start($fileArray);
			$content .= $this->cObj->cObjGetSingle($conf['image'], $conf['image.']);
			$i++;
		}
		$content .= $this->cObj->cObjGetSingle($conf['allgallerylink'], $conf['allgallerylink.']);
		return $content;
	}

	/**
	 * Get the Foldername or the Name of the TXT File in the Folder
	 * @param $filepath
	 * @param $value
	 * @return mixed
	 */
	protected function getGalleryTitle($theconf, $filepath, $value='', $typoscriptwrap = 0) {
		$gallerysorting = $this->conf['gallerysorting.']['split'] ? $this->conf['gallerysorting.']['split'] : 0;
		$gallerysortingsplitvar = $this->conf['gallerysorting.']['splitvar'] ? $this->conf['gallerysorting.']['splitvar'] : '_';

		if($gallerysorting == 1) {
			$galleryname = explode($gallerysortingsplitvar, $value);
			$galleryname = $galleryname[1];
		} else {
			$galleryname = $value;
		}

		// Gallery TXT-File
		$txtfile = reset(t3lib_div::getFilesInDir($filepath . $value, 'txt', true));
		if($txtfile) {
			$galleryname = str_replace('.txt', '', $txtfile);
			$galleryname = str_replace($filepath . $value . '/', '', $galleryname);
		}


		if($typoscriptwrap == 1) {
			$newConf = array(
				'gallerytitle' => $galleryname
			);
			$this->cObj->start($newConf);
			return $this->cObj->cObjGetSingle($this->conf[$theconf]['title'], $this->conf[$theconf]['title.']);
		} else {
			return $galleryname;
		}
	}

	/**
	 * Generate a Pagebrowser
	 * @param $numberOfPages
	 * @param $parameter
	 * @return string
	 */
	protected function getListGetPageBrowser($numberOfPages, $parameter) {
		// Get default configuration
		$GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_pagebrowse_pi1.']['pageParameterName'] = $parameter;
		$conf = $GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_pagebrowse_pi1.'];
		// Modify this configuration
		$conf += array(
			'pageParameterName' => $this->prefixId . '|page',
			'numberOfPages' => $numberOfPages,
		);
		// Get page browser
		$cObj = t3lib_div::makeInstance('tslib_cObj');
		/* @var $cObj tslib_cObj */
		$cObj->start(array(), '');
		return $cObj->cObjGetSingle('USER', $conf);
	}
}

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/st_simplegallery/pi1/class.tx_stsimplegallery_pi1.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/st_simplegallery/pi1/class.tx_stsimplegallery_pi1.php']);
}

?>
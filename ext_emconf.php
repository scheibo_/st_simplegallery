<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "st_simplegallery".
 *
 * Auto generated 26-06-2013 19:38
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
	'title' => 'Simple Gallery',
	'description' => '',
	'category' => 'plugin',
	'author' => 'Thomas Scheibitz',
	'author_email' => 'mail@kreativschmiede-eichsfeld.de',
	'shy' => '',
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'module' => '',
	'state' => 'beta',
	'internal' => '',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author_company' => 'Kreativschmiede Eichsfeld',
	'version' => '1.0.0',
	'constraints' => 
	array (
		'depends' => 
		array (
			'pagebrowse' => '1.3.3-9.9.9',
		),
		'conflicts' => 
		array (
		),
		'suggests' => 
		array (
		),
	),
	'_md5_values_when_last_written' => 'a:15:{s:9:"ChangeLog";s:4:"63c4";s:10:"README.txt";s:4:"ee2d";s:12:"ext_icon.gif";s:4:"e1e9";s:17:"ext_localconf.php";s:4:"2fe9";s:14:"ext_tables.php";s:4:"9109";s:12:"flexform.xml";s:4:"eb22";s:16:"locallang_db.xml";s:4:"6cd1";s:17:"locallang_tca.php";s:4:"b22e";s:19:"doc/wizard_form.dat";s:4:"787f";s:20:"doc/wizard_form.html";s:4:"4a99";s:36:"pi1/class.tx_stsimplegallery_pi1.php";s:4:"e24d";s:17:"pi1/locallang.xml";s:4:"ce9b";s:16:"static/style.css";s:4:"3e4b";s:37:"static/st_simplegallery/constants.txt";s:4:"d41d";s:33:"static/st_simplegallery/setup.txt";s:4:"fac4";}',
	'suggests' => 
	array (
	),
);

?>